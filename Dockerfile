# Build an instance with just java and the fat jar file
FROM eclipse-temurin:11-alpine

# Install update-ca-certificates for self-signed certificate support
RUN apk update && apk add ca-certificates && rm -rf /var/cache/apk/*

# Add the fat jar itself
ADD bin/* instance.jar

ENTRYPOINT ["/opt/java/openjdk/bin/java", "-jar", "instance.jar"]
